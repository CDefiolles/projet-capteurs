"""
Liste de nouvelles commandes flask liées à la BD
Exemple : dans le terminal, "flask syncdb" drop all et creer les tables
"""
from datetime import datetime

import click
import yaml
from yaml.scanner import ScannerError

from . import id_arduino_est_valide, id_capteur_est_valide, donnees_sont_valide, Donnee_physique, \
    donnees_gps_sont_valide, Donnee_gps
from .app import app, db


@app.cli.command()
def syncdb():
    """ Drops and Creates the tables"""
    db.drop_all()
    db.create_all()


@app.cli.command()
@click.argument('url_donnee')
def insertion_donnees(url_donnee):
    """ Populates the tables with data """
    # Lecture du fichier yml
    try:
        with open(url_donnee, 'rt', encoding="utf8") as yml:
            donnees_arduino = yaml.safe_load(yml)
        # donnees_arduino = yaml.safe_load(open(url_donnee))
        for arduino in donnees_arduino:
            # Vérification que l'id de l'arduino dans le fichier est un int
            id_arduino = arduino["id_arduino"]
            if id_arduino_est_valide(id_arduino, int):
                for capteur in arduino["capteurs"]:
                    # Vérification que l'id du capteur dans le fichier est un int
                    id_capteur = f"{id_arduino}A{capteur['id_capteur']}"
                    if id_capteur_est_valide(id_capteur, str):
                        if "donnees_physique" in capteur:
                            for donnees_physique in capteur["donnees_physique"]:
                                if donnees_sont_valide(donnees_physique, id_capteur):
                                    # Si tous les champs existent et sont valides
                                    date_time = [int(elem) for elem in donnees_physique["date_donnee"].split("-")]
                                    donnee_actuelle = Donnee_physique(nom_donnee=donnees_physique["nom_donnee"],
                                                                      unite_donnee=donnees_physique["unite_donnee"],
                                                                      date_donnee=datetime(date_time[0],
                                                                                           date_time[1],
                                                                                           date_time[2],
                                                                                           date_time[3],
                                                                                           date_time[4],
                                                                                           date_time[5]),
                                                                      val_donnee=donnees_physique["val_donnee"],
                                                                      id_capteur=id_capteur)
                                    db.session.add(donnee_actuelle)
                                    db.session.commit()
                        if "donnees_gps" in capteur:
                            for donnees_gps in capteur["donnees_gps"]:
                                if donnees_gps_sont_valide(donnees_gps):
                                    # Si tous les champs existent et sont valides
                                    date_time = [int(elem) for elem in donnees_gps["date_donnee"].split("-")]
                                    donnee_gps_actuelle = Donnee_gps(date_donnee=datetime(date_time[0],
                                                                                          date_time[1],
                                                                                          date_time[2],
                                                                                          date_time[3],
                                                                                          date_time[4],
                                                                                          date_time[5]),
                                                                     latitude=donnees_gps["latitude"],
                                                                     longitude=donnees_gps["longitude"],
                                                                     id_capteur=id_capteur)
                                    # print(f"latitude : {donnees_gps['latitude']}, longitude : {donnees_gps[
                                    # 'longitude']}")
                                    db.session.add(donnee_gps_actuelle)
                                    db.session.commit()
    except ScannerError as error:
        print(f"Exception : {error}")
        print(f"Le fichier \"{url_donnee}\" n'a pas pu être scanné. Les données dont dispose le fichier n'ont pas été "
              f"inséré en BD")
