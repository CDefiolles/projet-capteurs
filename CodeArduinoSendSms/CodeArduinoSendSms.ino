#include <DFRobot_sim808.h>
#include <sim808.h>

#include <Arduino.h>
#include <avr/io.h>

#include <IUTO_sim808C.h>

#define mySerial Serial1

//########################################
#define PHONE_NUMBER "0652686975"  //Put your phone number here!!
#define idArduino "1" //Put the id of the Arduino here
//########################################

#define Powerkey 12  // PIN used to  wake up shield
#define SIM8008rate 9600 //only 9600 works... Why??
#define STR_LENGTH 200
#define JUMP 3.0
#define MARGINTIME 120

char str[STR_LENGTH];
char data[STR_LENGTH];

IUTO_SIM808 sim808(&mySerial);

//******Program states
bool  ledON = false; //switch on led
bool  ledOFF = false; //switch off led
unsigned long starttime;
volatile bool  testSMS=false;
volatile bool  testGPS=false;
bool  send_sens = false; // Prepare  GPS data into str char*   (to be send by SMS)
volatile int  seconds = 0; //make it volatile because it is used inside the interrupt
volatile bool testSim808=false;
unsigned char count=1;
int humid = 80;
volatile bool margin=false;
volatile int marginTime=MARGINTIME;

void setup() {

//******** if arduino is connected to a PC *************
#ifdef SERVER
  Serial.begin(SIM8008rate); //does not need to be same rate than mySerial so may be changed to other values
  while (!Serial);
   GG_DEBUG_PRINTLN("Sim808 is connected to PC");
    wink(10, 50);
#endif

  
  pinMode(Powerkey, OUTPUT); // needed to wake up shield

 
//******** communication to  shield 808 ************* 
 mySerial.begin(SIM8008rate);
  while (!mySerial);

  wink(10, 50);
  wink(count++, 500);
  

  
  //******** Initialize sim808 module *************
  ResetSim808(true); //try to wake up shield  ; true-> a SMS will be send at the end of this process
 
  
  sim808_clean_buffer(str,sizeof(str));

  myinterrupts(1);  //1Hz
}

void(* resetFunc) (void) = 0; //declare reset function @ address 0

ISR(TIMER1_COMPA_vect)          // timer compare interrupt service routine
{
  // Do every nbsec second 
  seconds++;
    if (seconds > 4500){
    testSim808=true;
    seconds =0;}
    else{ 
     if(seconds %marginTime ==0){margin=true;}
     if(seconds %20 ==0){testGPS=true;}
      else if(seconds %10 == 0){testSMS=true;}
    }
}


void loop() {
  delay(1);

  if (testSim808){
    GG_DEBUG_PRINTLN("Test if 808 is still OK"); 
    if (!sim808.testSIM808(20000,true)) {
      GG_DEBUG_PRINTLN("Lost Connection, decide to reset 808... "); 
      ResetSim808(true) ;
      GG_DEBUG_PRINTLN("reset done"); 
    }
    testSim808=false;
   }
  
  if (testSMS){ wink(2, 500); 
    GG_DEBUG_PRINTLN("testSMS"); 
    tryReadSMS();
    testSMS = false;
    }
    
if (testGPS){
    if (tryReadGPS()){
    ReadDataGPS();
    if(humid<15){
      humid += random(65,80);
    }
    int dataMinus = random(1,5);
    int dataPlus = random(1,3);
    humid = humid + dataPlus - dataMinus;
    snprintf (str, STR_LENGTH,"Data%s|3|Localisation$%sHumidity$%s", idArduino, data, humid);
    sim808_clean_buffer(data,sizeof(data));
    sim808.sendSMS(PHONE_NUMBER, str);
    sim808_clean_buffer(str,sizeof(str));
    }
    testGPS = false; 
  }
}
//#########################################################################################


 void wink(int nb, int wait) {
  for (int i = 0; i < nb; i++) {
    digitalWrite(LED_BUILTIN, LOW);
    delay(wait);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(wait);

  }
}

void ResetSim808(bool sendaSMS) {
 
//  //******** Initialize sim808 data rate but does not work with other values than 9600 :-( *************

 

   wink(count++, 500);
 
 GG_DEBUG_PRINTLN("Init 808"); 
 sim808.powerReset(Powerkey);  
 
   wink(count++, 500);
   
 GG_DEBUG_PRINTLN("Init rate with 808"); 
while(!sim808.rate(SIM8008rate)) ;
 
 
 { GG_DEBUG_PRINTLN("Turn Rate");
//
   mySerial.flush();
   mySerial.begin(SIM8008rate);
    while( mySerial.available())  {GG_DEBUG_PRINTLN("Purge");mySerial.read();}
    //delay(1000);
  }

//******** Wait a connection to the shield establised *************  
while(!sim808.testSIM808(1000,false)) {delay(1000);wink(1, 200);}
  
sim808.deleteAllSMS(10000); // Delete message to free memory, may be important not to saturate memory
  
 wink(count++, 500);
 GG_DEBUG_PRINTLN("Turn on the GPS power");
//
//  //************* Turn on the GPS power************
 
 while(!sim808.poweronGPS() ) {delay(1000);wink(1, 200);}
   
   ; // wake up GPS GNSS module module
 
 wink(count++, 500);
  
if (sendaSMS){
snprintf (str,STR_LENGTH, "OK, please  send SMS message to me, or give me order by serial");
  GG_DEBUG_PRINTLN("send the SMS");
    sim808.sendSMS(PHONE_NUMBER, str);
     sim808_clean_buffer(str,sizeof(str));
     }
  wink(count++, 500);
}

void myinterrupts(unsigned long frequency){// not less than 0.1 hz
  noInterrupts();         // disable all interrupts

  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1  = 0;

  //OCR1A = 625;    // 10ms
  //OCR1A =1250000;
  OCR1A = (unsigned long)(62500UL / frequency) ;         // compare match register 16MHz/256/100Hz
        // 16000000/256/100 
          //100Hz= 100/s =toutes les  0,01s = 10ms
        // This value makes ISR execute every 10ms

  TCCR1B |= (1 << WGM12);     // CTC mode
  TCCR1B |= (1 << CS12);      // 256 prescaler 
  TIMSK1 |= (1 << OCIE1A);    // enable timer compare interrupt

  interrupts();  
}

void tryReadSMS() {

if(sim808.readSMS(1, str, STR_LENGTH,10000))
  { 
    sim808.deleteAllSMS(7000); // Delete message to free memory, may be important not to saturate memory

    // depends of value into the SMS we choose a suitable state for the program
    if (str[0] == '*')ledON = true;
    if (str[0] == '-')ledOFF = true;
    if (str[0] == '@')send_sens = true;
    if (str[0] == 'D'){Serial.print(str);};
    
  }
sim808_clean_buffer(str,sizeof(str));  
}


char ReadDataGPS() {
  // compose a string with choosen data
  char str_temp_lat[30];
  dtostrf(sim808.latDec, 9, 6, str_temp_lat);

  char str_temp_lon[30];
  dtostrf(sim808.longDec, 9, 6, str_temp_lon);

  snprintf (data, STR_LENGTH,"Latitude:%s,Longitude:%s",str_temp_lat,str_temp_lon);
        
  return data;
}



bool tryReadGPS() { //this is called until enough GPS data are readed by arduino to have a geographic  position.
  // Partial information are saved into a global variable so they are not lost from calls to calls to this function.

 if (  sim808.waitGPSready(2)) {
  //the arduino  have a new geographic position!
 wink(10, 50);
return true;
  }
return false;
//the arduino may have a new geographic position
}
