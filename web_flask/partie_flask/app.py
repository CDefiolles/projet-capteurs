"""
Application
"""
from flask import Flask
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_mail import Mail

app = Flask(__name__)
app.jinja_options['extensions'].append("jinja2.ext.do")
mail=Mail(app)
Bootstrap(app)
login_manager = LoginManager(app)
login_manager.login_view = "login"
app.config.update(SEND_FILE_MAX_AGE_DEFAULT=0)
app.config.from_envvar("PROJET_CAPTEURS_SETTINGS")
db = SQLAlchemy(app)
mail = Mail(app)
