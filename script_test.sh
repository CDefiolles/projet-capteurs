pylint web_flask
xenon -a B web_flask
coverage run web_flask/tests.py
coverage report -m --omit="venv/*"
