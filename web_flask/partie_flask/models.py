"""
Conception des tables et association de la base de données
"""
from flask_login import UserMixin
from sqlalchemy.dialects import mysql

from .app import db, login_manager


@login_manager.user_loader
def load_user(pseudo_utilisateur):
    return Utilisateur.query.get(pseudo_utilisateur)


utilisateur_administre_groupe = db.Table("utilisateur_administre_groupe",
                                         db.Column("pseudo_utilisateur", db.String(20),
                                                   db.ForeignKey("utilisateur.pseudo_utilisateur"),
                                                   primary_key=True),
                                         db.Column("id_groupe", db.Integer,
                                                   db.ForeignKey("groupe.id_groupe"),
                                                   primary_key=True)
                                         )

groupe_capteur = db.Table("groupe_possede_capteur",
                          db.Column("id_capteur", db.String(21), db.ForeignKey("capteur.id_capteur"),
                                    primary_key=True),
                          db.Column("id_groupe", db.Integer, db.ForeignKey("groupe.id_groupe"),
                                    primary_key=True)
                          )


class Utilisateur(db.Model, UserMixin):
    """Table BD"""
    pseudo_utilisateur = db.Column(db.String(20), primary_key=True)
    nom_utilisateur = db.Column(db.String(140))
    prenom_utilisateur = db.Column(db.String(140))
    mdp_utilisateur = db.Column(db.String(140))
    mail_utilisateur = db.Column(db.String(140))
    img_utilisateur = db.Column(db.String(140))
    abonnements = db.relationship("Souscrit_abonnement", back_populates="utilisateur")
    operations = db.relationship("Operation", backref="pseudo_utilisateur", lazy=True)
    utilisateur_appartient_groupe = db.relationship("Utilisateur_appartient_groupe", back_populates="utilisateur")
    groupes_administres = db.relationship("Groupe", secondary=utilisateur_administre_groupe)
    arduinos = db.relationship("Arduino", backref="pseudo_utilisateur", lazy=True)

    def get_id(self):
        return self.pseudo_utilisateur

    def __repr__(self):
        return "pseudo:" + self.pseudo_utilisateur + " " + "véritable nom :" + \
               self.nom_utilisateur + " " + self.prenom_utilisateur


class Operation(db.Model):
    """Table BD"""
    id_operation = db.Column(db.Integer, primary_key=True)
    date_operation = db.Column(db.DateTime)
    information_operation = db.Column(db.String(140))
    utilisateur = db.Column(db.String(20), db.ForeignKey("utilisateur.pseudo_utilisateur"))

    def __repr__(self):
        return f"id_operation : {self.id_operation}, date_operation : {self.date_operation}, " \
               f"information_operation : {self.information_operation} "


class Abonnement(db.Model):
    """Table BD"""
    id_abonnement = db.Column(db.Integer, primary_key=True)
    nom_abonnement = db.Column(db.String(140))
    utilisateurs = db.relationship("Souscrit_abonnement", back_populates="abonnement")

    def __repr__(self):
        return f"id_abonnement : {self.id_abonnement}, nom_abonnement : {self.nom_abonnement}"


class Donnee_physique(db.Model):
    """Table BD"""
    id_donnee = db.Column(db.Integer, primary_key=True)
    nom_donnee = db.Column(db.String(64))
    unite_donnee = db.Column(db.String(140))
    date_donnee = db.Column(db.DateTime)
    val_donnee = db.Column(db.Float)
    id_capteur = db.Column(db.String(21), db.ForeignKey("capteur.id_capteur"))

    def __repr__(self):
        return f"id_donnee : {self.id_donnee}, nom_donnee : {self.nom_donnee}, type_donnee : {self.unite_donnee}, " \
               f"date_donnee : {self.date_donnee}, val_donnee : {self.val_donnee}," \
               f" capteur : {self.id_capteur} "


class Donnee_gps(db.Model):
    """Table BD"""
    id_donnee = db.Column(db.Integer, primary_key=True)
    date_donnee = db.Column(db.DateTime)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    id_capteur = db.Column(db.String(21), db.ForeignKey("capteur.id_capteur"))

    def __repr__(self):
        return f"id_donnee : {self.id_donnee}, " \
               f"date_donnee : {self.date_donnee}, latitude : {self.latitude}, longitude : {self.latitude}," \
               f" capteur : {self.id_capteur} "


class Arduino(db.Model):
    """Table BD"""
    id_arduino = db.Column(db.Integer, primary_key=True)
    nom_arduino = db.Column(db.String(64))
    utilisateur = db.Column(db.String(20), db.ForeignKey("utilisateur.pseudo_utilisateur"))
    capteurs = db.relationship("Capteur", backref="id_arduino", lazy=True)


class Capteur(db.Model):
    """Table BD"""
    id_capteur = db.Column(db.String(21), primary_key=True)
    nom_capteur = db.Column(db.String(140))
    date_installation_capteur = db.Column(db.DateTime)
    arduino = db.Column(db.Integer, db.ForeignKey("arduino.id_arduino"))
    donnees_physique = db.relationship("Donnee_physique", backref="capteur")
    droit_role_capteur = db.relationship("Droit_role_capteur", back_populates="capteur")
    groupes = db.relationship("Groupe", secondary=groupe_capteur)

    def __repr__(self):
        return f"id_capteur : {self.id_capteur}, nom_capteur : {self.nom_capteur}, " \
               f"date_installation_capteur : {self.date_installation_capteur}, id_arduino : {self.arduino}"


class Groupe(db.Model):
    """Table BD"""
    id_groupe = db.Column(db.Integer, primary_key=True)
    nom_groupe = db.Column(db.String(140))
    est_public = db.Column(db.Boolean(), default=False)
    utilisateur_appartient_groupe = db.relationship("Utilisateur_appartient_groupe", back_populates="groupe")
    administrateurs = db.relationship("Utilisateur", secondary=utilisateur_administre_groupe)
    roles = db.relationship("Role", backref="groupe", lazy=True)
    capteurs = db.relationship("Capteur", secondary=groupe_capteur)

    def __repr__(self):
        return f"id_groupe : {self.id_groupe}, nom_groupe : {self.nom_groupe}"


class Role(db.Model):
    """Table BD"""
    id_role = db.Column(db.Integer, primary_key=True)
    nom_role = db.Column(db.String(140))
    id_groupe = db.Column(db.Integer, db.ForeignKey("groupe.id_groupe"))
    utilisateur_appartient_groupe = db.relationship("Utilisateur_appartient_groupe", back_populates="role")
    droit_role_capteur = db.relationship("Droit_role_capteur", back_populates="role")
    droits_groupe = db.relationship("Droit_role_groupe", back_populates="role")

    def __repr__(self):
        return f"id_role : {self.id_role}, nom_role : {self.nom_role}, id_groupe : {self.id_groupe}"


class Droit_groupe(db.Model):
    """Table BD"""
    id_droit = db.Column(db.Integer, primary_key=True)
    nom_droit = db.Column(db.String(32))
    description_droit = db.Column(db.String(140))
    roles = db.relationship("Droit_role_groupe", back_populates="droit_groupe")

    def __repr__(self):
        return f"id_droit : {self.id_droit}"


class Droit_capteur(db.Model):
    """Table BD"""
    id_droit = db.Column(db.Integer, primary_key=True)
    nom_droit = db.Column(db.String(32))
    description_droit = db.Column(db.String(140))
    droit_role_capteur = db.relationship("Droit_role_capteur", back_populates="droit_capteur")

    def __repr__(self):
        return f"id_droit : {self.id_droit}"


class Souscrit_abonnement(db.Model):
    """Table BD"""
    pseudo_utilisateur = db.Column(db.String(20), db.ForeignKey(
        'utilisateur.pseudo_utilisateur'), primary_key=True)
    id_abonnement = db.Column(db.Integer, db.ForeignKey(
        'abonnement.id_abonnement'), primary_key=True)
    nbr_capteur = db.Column(db.Integer)
    duree_engagement = db.Column(db.String(140))
    prix_mensuel = db.Column(db.Integer)
    utilisateur = db.relationship("Utilisateur", back_populates="abonnements")
    abonnement = db.relationship("Abonnement", back_populates="utilisateurs")

    def __repr__(self):
        return f"pseudo_utilisateur : {self.pseudo_utilisateur}," \
               f" id_abonnement : {self.nbr_capteur}, duree_engagement : {self.duree_engagement}," \
               f" prix_mensuel : {self.prix_mensuel} "


class Droit_role_groupe(db.Model):
    """Table BD"""
    id_role = db.Column(db.Integer, db.ForeignKey("role.id_role"), primary_key=True)
    id_droit_groupe = db.Column(db.Integer, db.ForeignKey("droit_groupe.id_droit"), primary_key=True)
    droit_actif = db.Column(mysql.TINYINT)
    role = db.relationship("Role", back_populates="droits_groupe")
    droit_groupe = db.relationship("Droit_groupe", back_populates="roles")


class Droit_role_capteur(db.Model):
    """Association object permettant le triple many-to-many entre un role, un droit_capteur et un capteur"""
    id = db.Column(db.Integer, primary_key=True)

    # les 3 clés étrangères faisant références aux 3 entités
    id_role = db.Column("id_role", db.Integer, db.ForeignKey("role.id_role"),
                        primary_key=True)
    id_droit_capteur = db.Column(db.Integer, db.ForeignKey("droit_capteur.id_droit"), primary_key=True)
    id_capteur = db.Column(db.String(21), db.ForeignKey("capteur.id_capteur"), primary_key=True)

    # l'attribut spécifiant si le droit est activé ou non pour ce rôle
    droit_actif = db.Column(mysql.TINYINT)

    # on précise que chaque assemblement de ces 3 clés est unique dans la BD
    __table_args__ = (db.UniqueConstraint(id_role, id_droit_capteur, id_capteur),)

    droit_capteur = db.relationship("Droit_capteur", back_populates="droit_role_capteur")
    role = db.relationship("Role")
    capteur = db.relationship("Capteur", back_populates="droit_role_capteur")


class Utilisateur_appartient_groupe(db.Model):
    """Association object permettant le triple many-to-many entre un utilisateur, un groupe et un rôle"""
    id = db.Column(db.Integer, primary_key=True)

    # les 3 clés étrangères faisant références aux 3 entités
    pseudo_utilisateur = db.Column(db.String(20), db.ForeignKey("utilisateur.pseudo_utilisateur"), nullable=False)
    id_groupe = db.Column(db.Integer, db.ForeignKey("groupe.id_groupe"), nullable=False)
    id_role = db.Column(db.Integer, db.ForeignKey("role.id_role"), nullable=False)

    # on précise que chaque assemblement de ces 3 clés est unique dans la BD
    __table_args__ = (db.UniqueConstraint(pseudo_utilisateur, id_groupe, id_role),)

    utilisateur = db.relationship("Utilisateur", back_populates="utilisateur_appartient_groupe")
    groupe = db.relationship("Groupe")
    role = db.relationship("Role", back_populates="utilisateur_appartient_groupe")

    def __repr__(self):
        return f"L'utilisateur {self.pseudo_utilisateur} appartient au groupe d'id {self.id_groupe} en tant que rôle " \
               f"{self.id_role}"
