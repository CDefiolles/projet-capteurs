"""
Routes
"""
import datetime
from dateutil.relativedelta import relativedelta
import random
import string
from hashlib import sha256
from random import choice

import folium
from flask import render_template, url_for, redirect, flash, request
from flask_login import login_user, logout_user, current_user, login_required
from flask_wtf import FlaskForm
from wtforms import StringField, HiddenField, PasswordField, SubmitField, BooleanField,SelectField
from wtforms.validators import DataRequired, Length, ValidationError, Email, EqualTo
from wtforms_sqlalchemy.fields import QuerySelectField
from flask_mail import Mail, Message
from .app import app, mail
from .query import *

list_findelien_reinit_mdp = []


class RegistrationForm(FlaskForm):
    """Classe de formulaire inscription"""
    pseudo = StringField('Pseudo', validators=[DataRequired(), Length(min=2, max=32)])
    nom = StringField('Nom', validators=[DataRequired(), Length(min=2, max=32)])
    prenom = StringField('Prénom', validators=[DataRequired(), Length(min=2, max=32)])
    email = StringField("Email", validators=[DataRequired(), Email()])
    mot_de_passe = PasswordField('Mot de passe', validators=[DataRequired(), Length(min=6, max=32)])
    confirm_mot_de_passe = PasswordField('Confirmer mot de passe',
                                         validators=[DataRequired(), Length(min=6, max=32), EqualTo("mot_de_passe")])
    submit = SubmitField("S'inscrire")
    next = HiddenField()

    def validate_pseudo(self, pseudo):
        pseudo_utilisateur = Utilisateur.query.get(pseudo.data)
        if pseudo_utilisateur:
            raise ValidationError("Ce pseudonyme est déjà utilisé")
        return True


class LoginForm(FlaskForm):
    """Classe de formulaire login"""
    username = StringField('Pseudonyme', validators=[DataRequired(), Length(min=2, max=20)])
    password = PasswordField('Mot de passe', validators=[DataRequired(), Length(min=6, max=32)])
    submit = SubmitField("Se connecter")
    next = HiddenField()

    def get_authenticated_user(self):
        utilisateur = Utilisateur.query.get(self.username.data)
        if utilisateur is None:
            return None
        m = sha256()
        m.update(self.password.data.encode())
        passwd = m.hexdigest()
        if passwd == utilisateur.mdp_utilisateur:
            return utilisateur
        return None


# def verif_capteurs(self):
#    cap=Capteur.query.get(self.ref.data)
#   if cap:
#      raise ValidationError("Ce capteur a deja été enregistré")
# return True

class ComparerForm(FlaskForm):
    """Formulaire pour fournir les capteurs à comparer (depuis la légende)"""
    submit = SubmitField("Comparer")
    next = HiddenField()


class GroupeForm(FlaskForm):
    """Formulaire pour la création d'un groupe"""
    nom = StringField('Nomgroupe')
    est_public = BooleanField('Rendre le groupe public')
    submit = SubmitField("creerGroupe")


class ArduinoForm(FlaskForm):
    """Formulaire pour l'ajout d'une carte arduino"""

    nom_arduino = StringField('arduino')
    submit = SubmitField("creerArduino")


class SupprimerMembreGroupe(FlaskForm):
    """Formulaire pour la suppression d'un membre d'un groupe"""
    submit = SubmitField("Retirer ce membre")
    next = HiddenField()


class QuitterGroupe(FlaskForm):
    """Formulaire pour quitter un groupe"""
    submit = SubmitField("Quitter ce groupe")
    next = HiddenField()


class ResetPassword(FlaskForm):
    """Formulaire pour envoyer une requête de reinitialisation de mot de passe"""
    pseudo = StringField('pseudo')
    submit = SubmitField("ResetPassword")


class SendInvit(FlaskForm):
    """Formulaire pour envoyer une invitation"""
    pseudo = StringField('pseudo')
    id = StringField('id')
    submit = SubmitField("SendInvit")


class RetirerCapteurGroupe(FlaskForm):
    """Formulaire pour retirer un capteur d'un groupe"""
    submit = SubmitField("Retirer ce capteur")
    next = HiddenField()

class ChangeRole(FlaskForm):
    """Formulaire pour changer le rôle d'un utilisateur"""
    pseudo=StringField('pseudo')
    id_groupe=StringField('id_groupe')
    listerole=SelectField(choices=["Lecteur","Ecrivain","Administrateur"])
    submit=SubmitField("changer de rôle") 

class RetirerRole(FlaskForm):
    """Formulaire pour enlever le rôle à une personne"""
    submit = SubmitField("Retirer ce capteur")
    next = HiddenField()

def send_msg_invitation(pseudo_sender, pseudo_receveur, mail_utilisateur, id_groupe):
    random_fin_de_lien = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
    msg = Message('Invitation in a group', sender='BioCapteursnoreply@gmail.com', recipients=[mail_utilisateur])
    msg.body = "Hello" + pseudo_receveur + ", recently," + pseudo_sender + \
               "has invited you in a group to study some results of capteurs\n"
    msg.body += "Click on the link below to accept the invitation:\n"
    msg.body += "http://localhost:5000/groupes/" + str(
        id_groupe) + "/invitation/" + pseudo_receveur + "/" + random_fin_de_lien + "\n"
    msg.body += "Have a nice day"
    mail.send(msg)


def send_msg_reset_mdp(pseudo_utilisateur, mail_utilisateur):
    user = Utilisateur.query.get(pseudo_utilisateur)
    if user.mail_utilisateur == mail_utilisateur:
        random_fin_de_lien = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
        list_findelien_reinit_mdp.append(random_fin_de_lien)
        msg = Message('Reset your password', sender='BioCapteursnoreply@gmail.com', recipients=[mail_utilisateur])
        msg.body = "Hello" + pseudo_utilisateur + "you decided to reset your password\n"
        msg.body += "Click on the link below to reset your password:\n"
        msg.body += "http://localhost:5000//profil/reinitmdp/" + pseudo_utilisateur + "/" + random_fin_de_lien
        mail.send(msg)
        

@app.route('/profil/reinitmdp/<pseudo_utilisateur>/<string:nom>', methods=['GET', 'POST'])
def page_reinitialise_mdp(pseudo_utilisateur, nom):
    if nom in list_findelien_reinit_mdp:
        list_findelien_reinit_mdp.remove(nom)
        user = Utilisateur.query.get(pseudo_utilisateur)
        new_mdp = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
        mdp = sha256()
        mdp.update(new_mdp.encode())
        new_mdp_a_montrer = new_mdp
        # new_mdp.encode()
        user.mdp_utilisateur = mdp.hexdigest()
        db.session.commit()
        return render_template("reinitialiser_mdp.html", titre="Réinitialisation du mot de passe",
                               new_mdp=new_mdp_a_montrer)
    else:
        return "Vous n'êtes pas autorisé à faire cette action"


@app.route('/groupes/<id_groupe>/invitation/<pseudo_utilisateur>/<string:nom>')
def page_accepter_invitation(pseudo_utilisateur, id_groupe, nom):
    groupe_appartenant = Groupe.query.get(id_groupe)
    role = Role.query.filter(Role.id_groupe == id_groupe, Role.nom_role == "Défaut").first()
    to_insert = Utilisateur_appartient_groupe(pseudo_utilisateur=pseudo_utilisateur, id_groupe=id_groupe,
                                              id_role=role.id_role)
    db.session.add(to_insert)
    db.session.commit()
    return render_template("accepter_invitation.html", pseudo_utilisateur=pseudo_utilisateur,
                           groupe_nom=groupe_appartenant.nom_groupe)


@app.route('/', methods=['GET', 'POST'])
def home():
    """Retourne Home"""
    # send_msg_reset_mdp("essai","giordino61@gmail.com")
    if current_user.is_authenticated:
        # formulaire pour la checklist
        form_comparer = ComparerForm()

        # Partie temporaire pour créer un arduino et un capteur d'utilisateur, pour tester la carte

        if len(Arduino.query.all()) == 0:
            id_arduino1 = creer_arduino("arduino1", current_user.get_id())
            creer_capteur("1A1", "capteurTest", id_arduino1)
            creer_capteur("1A2", "capteurTest2", id_arduino1)

            id_arduino2 = creer_arduino("arduino2", current_user.get_id())
            creer_capteur("2A1", "capteurTest3", id_arduino2)
            creer_capteur("2A2", "capteurTest4", id_arduino2)

            # Partie temporaire pour créer un groupe avec pour membre et administrateur l'utilisateur actuel
            id_groupe1 = creer_groupe("groupe1", current_user.get_id())
            id_groupe2 = creer_groupe("groupe2", current_user.get_id())

            # Partie temporaire pour ajouter le capteur au groupe
            modifier_add(Groupe, nom_clef_primaire="id_groupe", valeur_clef_primaire=id_groupe1,
                         dico_donnees={"capteurs": Capteur.query.get("1A1")})
            modifier_add(Groupe, nom_clef_primaire="id_groupe", valeur_clef_primaire=id_groupe2,
                         dico_donnees={"capteurs": Capteur.query.get("1A2")})
            modifier_add(Groupe, nom_clef_primaire="id_groupe", valeur_clef_primaire=id_groupe1,
                         dico_donnees={"capteurs": Capteur.query.get("2A1")})
            modifier_add(Groupe, nom_clef_primaire="id_groupe", valeur_clef_primaire=id_groupe2,
                         dico_donnees={"capteurs": Capteur.query.get("2A2")})

        # creation d'un dico groupe : [capteurs] pour la légende
        dico_groupe_capteurs = get_dico_groupe_capteurs(current_user.get_id())

        return render_template("home.html", title="Accueil", dico_groupe_capteurs=dico_groupe_capteurs,
                               form_comparer=form_comparer)

    return render_template("home.html", title="Accueil")


# Si un utilisateur non connecté tente d'accéder à une page qui nécessite la connexion
# cette fonction est appelée et il est renvoyé vers la page de connexion
@login_manager.unauthorized_handler
def unauthorized_callback():
    flash("Vous devez être connecté pour accéder à cette page", "info")
    return redirect(url_for('page_connexion'))


# Si un utilisateur tente d'accéder à une page qui nécessite n'authorise pas le GET
# cette fonction est appelée et il est renvoyé vers la page d'accueil'
@app.errorhandler(405)
def method_not_allowed(e):
    return redirect(url_for("home"))


@app.route('/map_capteur/<pseudo_utilisateur>', methods=['GET'])
@login_required
def page_map_capteur(pseudo_utilisateur):
    if current_user.get_id() == pseudo_utilisateur:
        ensemble_id_capteurs = set()
        for elem in request.args.keys():  # récupération des capteurs cochés
            ensemble_id_capteurs.add(elem)

        # creation des infos de markers
        tooltip = "Cliquer pour plus d'informations"
        colors = [
            'red',
            'blue',
            'gray',
            'darkred',
            'lightred',
            'orange',
            'beige',
            'green',
            'darkgreen',
            'lightgreen',
            'darkblue',
            'lightblue',
            'purple',
            'darkpurple',
            'pink',
            'cadetblue',
            'lightgray',
            'black'
        ]

        # Création des objets markers
        latitude_liste = []
        longitude_liste = []
        objets_marker = []

        for id_capteur in ensemble_id_capteurs:
            latitude = Donnee_gps.query.filter(Donnee_gps.id_capteur == id_capteur) \
                .order_by(Donnee_gps.id_donnee.desc()) \
                .with_entities(Donnee_gps.latitude).first()
            longitude = Donnee_gps.query.filter(Donnee_gps.id_capteur == id_capteur) \
                .order_by(Donnee_gps.id_donnee.desc()) \
                .with_entities(Donnee_gps.longitude).first()

            if latitude is not None and longitude is not None:
                latitude_liste.append(latitude[0])
                longitude_liste.append(longitude[0])
                marker1 = [latitude[0], longitude[0]]  # with_entities retourne un tuple donc on selectionne le premier
                nom_capteur = Capteur.query.filter(Capteur.id_capteur == id_capteur).with_entities(
                    Capteur.nom_capteur).first()[0]

                objet_marker = folium.Marker(marker1,
                                             popup=nom_capteur,
                                             tooltip=tooltip,
                                             icon=folium.Icon(color=choice(colors), icon="hdd"))
                objets_marker.append(objet_marker)

        # calcul des limites du 'rectangle' que crée les markers avec leur max et min
        if len(latitude_liste) >= 2 and len(longitude_liste) >= 2:  # si au moins 2 capteurs avec des coords sont cochés
            latitute_max = max(latitude_liste)
            longitude_max = max(longitude_liste)
            latitute_min = min(latitude_liste)
            longitude_min = min(longitude_liste)

            # calcul du centre de ce rectangle, ie le centre de la map
            latitute_centre = (latitute_max - latitute_min) / 2 + latitute_min
            longitude_centre = (longitude_max - longitude_min) / 2 + longitude_min

            # attribution du centre à la map
            coordonnees_depart = [latitute_centre, longitude_centre]
        elif len(latitude_liste) == 1 and len(longitude_liste) == 1:  # si un seul capteur avec des coords est coché
            # attribution du centre à la map
            coordonnees_depart = [latitude_liste[0], longitude_liste[0]]
        else:  # si aucun capteur avec des coords n'est coché
            # attribution du centre à la map avec une valeur par défaut (Orléans)
            coordonnees_depart = [47.902964, 1.909251]

        # creation de l'objet map
        map_capteur = folium.Map(location=coordonnees_depart)

        # insertion des markers
        for objet_marker in objets_marker:
            objet_marker.add_to(map_capteur)

        # sauvegarde de la map
        map_capteur.save("web_flask/partie_flask/templates/map_capteur.html")

        return render_template("map_capteur.html", title="Map")
    return redirect(url_for("home"))


@app.route('/groupe/<id>', methods=['GET','POST'])
@login_required
def page_groupe(id):
    form_supprimer_membre = SupprimerMembreGroupe()
    form_retirer_capteur_groupe = RetirerCapteurGroupe()
    form_changer_role=ChangeRole()
    form_suppr_role=RetirerRole()
    groupe = Groupe.query.get(id)
    capteurs_groupe = Capteur.query.filter(groupe_capteur.c.id_groupe == id,
                                           groupe_capteur.c.id_capteur == Capteur.id_capteur)

    dico_membres_roles_groupe = get_dico_membres_roles_groupe(id)
    if form_changer_role.validate_on_submit():
        id_role=creer_role(form_changer_role.listerole.data,form_changer_role.id_groupe.data)
        add_role(id_role,form_changer_role.pseudo.data,form_changer_role.id_groupe.data)
        return redirect(url_for("page_groupe", id=id))

    # Vérification du caractère administrateur de l'utilisateur connecté
    est_admin = False
    utilisateur_admin = db.session.query(utilisateur_administre_groupe) \
        .filter(utilisateur_administre_groupe.c.pseudo_utilisateur == current_user.get_id(),
                utilisateur_administre_groupe.c.id_groupe == id).first()
    if utilisateur_admin is not None:
        est_admin = True
    return render_template("groupe.html", groupe=groupe, capteurs_groupe=capteurs_groupe,
                           dico_membres_roles_groupe=dico_membres_roles_groupe, title="Groupe",
                           form_supprimer_membre=form_supprimer_membre, est_admin=est_admin,
                           form_retirer_capteur_groupe=form_retirer_capteur_groupe,form_changer_role=form_changer_role,form_retirer_role=form_suppr_role)


@app.route('/groupe/<id_groupe>/capteur/<id_capteur>/retirer', methods=['POST'])
@login_required
def page_retirer_capteur_groupe(id_groupe, id_capteur):
    print("entered view retirer")
    form_retirer_capteur_groupe = RetirerCapteurGroupe()
    if form_retirer_capteur_groupe.validate_on_submit():
        retirer_capteur_groupe(id_capteur=id_capteur, id_groupe=id_groupe)
    return redirect(url_for("page_groupe", id=id_groupe))

@app.route('/groupe/<id_groupe>/membres/<pseudo_utilisateur>/<id_role>',methods=['POST'])
@login_required
def page_retirer_role(id_role,id_groupe,pseudo_utilisateur):
    form_retirer_role=RetirerRole()
    if form_retirer_role.validate_on_submit():
        retirer_role_user_pour_un_groupe(pseudo_utilisateur,id_groupe,id_role)
    return redirect(url_for("page_groupe", id=id_groupe))
@app.route('/groupes', methods=['GET', 'POST'])
@login_required
def page_groupes():
    # recuperation des groupes auxquels l'utilisateur appartient + groupes public
    groupes = get_groupes_utilisateur(current_user.get_id())
    form_invit = SendInvit()
    if form_invit.validate_on_submit():
        user = Utilisateur.query.get(form_invit.pseudo.data)
        if user is None:
            flash("Nom introuvable","danger")
        else:
            mail_receveur = user.mail_utilisateur
            send_msg_invitation(current_user.get_id(), form_invit.pseudo.data, mail_receveur, form_invit.id.data)

    print(f"groupes : {groupes}")

    if groupes.first() is None:
        id_groupe_pre_selectione = None
    else:
        # par défaut la valeur de id_groupe_pre_selectione est le premier id dans la liste des groupes de l'utilisateur
        id_groupe_pre_selectione = groupes.with_entities(Groupe.id_groupe).first()[0]

    # si un id_groupe est trouvé dans l'url, alors il est utilisé comme valeur de id_groupe_pre_selectione
    for id_groupe in request.args:
        id_groupe_pre_selectione = id_groupe
    # l'attribut id_groupe_pre_selectione est l'id du groupe qui sera séléctionné (iframe groupe)
    # lors de l'arrivé sur la page

    # formulaire pour la checklist de la légende
    form_comparer = ComparerForm()

    # creation d'un dico groupe : [capteurs] pour la légende
    dico_groupe_capteurs = get_dico_groupe_capteurs(current_user.get_id())

    return render_template("groupes.html", title="Groupes", groupes=groupes,
                           id_groupe_pre_selectione=id_groupe_pre_selectione,
                           dico_groupe_capteurs=dico_groupe_capteurs, form_comparer=form_comparer,
                           form_invit=form_invit)


@app.route('/groupe/creation', methods=['GET', 'POST'])
@login_required
def page_groupe_creation():
    form = GroupeForm()
    if form.validate_on_submit():
        creer_groupe(form.nom.data, current_user.get_id(), form.est_public.data)
        return redirect(url_for('page_groupes'))
    return render_template("creer_groupe.html", form=form, title="Création de groupe")


@app.route('/groupe/membre/ajouter', methods=['GET', 'POST'])
@login_required
def page_groupe_membres_ajouter(pseudo_sender, pseudo_receveur, id_groupe):
    mail_receveur = Utilisateur.query.get(pseudo_receveur).mail_utilisateur
    send_msg_invitation(pseudo_sender, pseudo_receveur, mail_receveur, id_groupe)


@app.route('/groupe/<id_groupe>/membre/<pseudo_utilisateur>/supprimer', methods=['POST'])
@login_required
def page_groupe_membres_supprimer(pseudo_utilisateur, id_groupe):
    utilisateur_admin = db.session.query(utilisateur_administre_groupe) \
        .filter(utilisateur_administre_groupe.c.pseudo_utilisateur == current_user.get_id(),
                utilisateur_administre_groupe.c.id_groupe == id_groupe).first()
    # Verification que l'utilisateur qui effectue la suppression du membre est admin du groupe
    if utilisateur_admin is not None:
        retirer_utilisateur_groupe(pseudo_utilisateur=pseudo_utilisateur, id_groupe=id_groupe)
    return redirect(url_for("page_groupe", id=id_groupe))


@app.route('/groupe/membre/editer', methods=['GET', 'POST'])
@login_required
def page_groupe_membres_edition():
    return render_template("page_edit_suppr_membre.html", groupes=groupes_appartient())


@app.route('/groupe/capteurs', methods=['GET'])
@login_required
def page_groupe_capteurs():
    return 'liste capteurs groupe'


@app.route('/groupe/capteur', methods=['GET'])
@login_required
def page_groupe_capteur():
    return 'capteur particulier groupe'


@app.route('/groupe/capteur/supprimer', methods=['GET', 'POST'])
@login_required
def page_groupe_capteur_supprimer():
    return 'suppression capteur groupe'


@app.route('/groupe/<id_groupe>/roles', methods=['GET'])
@login_required
def page_groupe_roles(id_groupe):
    # récupération des rôles du groupe
    roles = Role.query.filter(Role.id_groupe == id_groupe)

    if roles.first() is None:
        id_role_pre_selectione = None
    else:
        # par défaut la valeur de id_role_pre_selectione est le premier id dans la liste des rôles du groupe
        id_role_pre_selectione = roles.with_entities(Role.id_role).first()[0]

    # si un id_role est trouvé dans l'url, alors il est utilisé comme valeur de id_role_pre_selectione
    for id_role in request.args:
        id_role_pre_selectione = id_role
    # l'attribut id_groupe_pre_selectione est l'id du groupe qui sera séléctionné (iframe groupe)
    # lors de l'arrivé sur la page

    return render_template("gestion_role.html", roles=roles, id_role_pre_selectione=id_role_pre_selectione)


@app.route('/role/<id_role>', methods=['GET', 'POST'])
@login_required
def page_droits(id_role):
    role = Role.query.get(id_role)
    # récupération du dictionnaire droit -> droit_actif (0 = False & 1 = True)
    dico_droits_groupe_droit_actif = get_dico_droits_groupe_droit_actif(id_role)
    return render_template("role_droits.html", role=role, dico_droits_groupe_droit_actif=dico_droits_groupe_droit_actif)


@app.route('/faq', methods=['GET'])
def page_faq():
    return render_template("faq.html", questions={
        "Partie capteurs": [("Comment fonctionne la transmission de données?",
                             "Les données récupérées par la sonde sont transmises à une carte arduino "
                             "qui les traite et les retransmet "
                             "via une antenne 2G à une autre carte arduino qui retraite "
                             "ces données pour les envoyer dans la base de données du site"),
                            ("Que faire si mes données ne s'affichent plus sur le site?",
                             "Ne vous inquiétez pas, si les données ont bien été transmises à la base de données, "
                             "nous avons toujours une sauvegarde pour restaurer ces dernières. Si cela persiste, "
                             "il se peut que la sonde ou la carte arduino est un problème. "
                             "Il faudra alors nous contacter.")]}, title="FAQ")


@app.route('/inscription', methods=['GET', 'POST'])
def page_inscription():
    """inscription"""
    form = RegistrationForm()
    if form.validate_on_submit():
        mdp = sha256()
        mdp.update(form.mot_de_passe.data.encode())
        utilisateur = Utilisateur(pseudo_utilisateur=form.pseudo.data, nom_utilisateur=form.nom.data,
                                  prenom_utilisateur=form.prenom.data, mdp_utilisateur=mdp.hexdigest(),
                                  mail_utilisateur=form.email.data)
        db.session.add(utilisateur)
        db.session.commit()

        flash("Votre compte a été créé avec succès. Vous pouvez maintenant vous connecter", "success")
        return redirect(url_for("page_connexion"))
    return render_template("inscription.html", title="Inscription", form=form)


@app.route('/connexion', methods=['GET', 'POST'])
def page_connexion():
    """connexion"""
    form = LoginForm()
    if not form.is_submitted():
        form.next.data = request.args.get("next")
    elif form.validate_on_submit():
        user = form.get_authenticated_user()
        if user:
            flash("Connexion réussie", "success")
            login_user(user)
            next_page = form.next.data or url_for("home")

            return redirect(next_page)
        flash("Connexion impossible. Veuillez vérifier votre pseudonyme et votre mot de passe", "danger")
    return render_template("connexion.html", title="Page de connexion", form=form)


# retourne la requête à utiliser pour le form
def groupes_appartient():
    groupes_appartient = Groupe.query.filter(Utilisateur_appartient_groupe.pseudo_utilisateur == current_user.get_id(),
                                             Utilisateur_appartient_groupe.id_groupe == Groupe.id_groupe)
    return groupes_appartient


def arduino_appartient():
    arduino_appartient = Arduino.query.filter(Arduino.utilisateur == current_user.get_id())
    return arduino_appartient


class CapteurForm(FlaskForm):
    # Classe de formulaire pour l'ajout d'un capteur
    ref = StringField('Reference', validators=[DataRequired(), Length(min=2, max=660)])
    nom = StringField('Nom', validators=[DataRequired(), Length(min=2, max=660)])
    type = StringField('Type', validators=[DataRequired(), Length(min=2, max=660)])
    groupe = QuerySelectField(query_factory=groupes_appartient, allow_blank=True, get_label='nom_groupe')
    arduino = QuerySelectField(query_factory=arduino_appartient, allow_blank=True, get_label='nom_arduino')
    gphysique = StringField('Grandeurphysique', validators=[DataRequired(), Length(min=2, max=660)])
    submit = SubmitField("CreerCapteur")


@app.route('/groupe/capteur/ajouter', methods=['GET', 'POST'])
@login_required
def page_groupe_capteur_ajout():
    form = CapteurForm()
    # print("dansleif")
    if form.validate_on_submit():
        print(
            f"form.ref:{form.ref.data}"
            f",form.nom:{form.nom.data}"
            f",form.type:{form.type.data}"
            f",form.groupe{form.arduino.data.id_arduino}")
        creer_capteur(form.ref.data, form.nom.data, form.arduino.data.id_arduino)
        modifier_add(Groupe, nom_clef_primaire="id_groupe", valeur_clef_primaire=form.groupe.data.id_groupe,
                     dico_donnees={"capteurs": Capteur.query.get(form.ref.data)})

        return redirect(url_for('page_groupe',id=form.groupe.data.id_groupe))

    # groupes_public = Groupe.query.filter(Groupe.est_public is True)
    # groupes = groupes_appartient.union(groupes_public).all()
    return render_template("ajout_capteurs.html", form=form)


@app.route('/connexion/mdpoublie', methods=['GET', 'POST'])
def page_mdp_oublie():
    form = ResetPassword()
    print(
        f"form.pseudo{form.pseudo.data}"
    )
    if form.validate_on_submit():
        mail_user=Utilisateur.query.get(form.pseudo.data).mail_utilisateur
        send_msg_reset_mdp(form.pseudo.data, mail_user)
        return redirect(url_for("home"))

    return render_template("mdp_oublie.html", title="Oublie de mot de passe", form=form)


@app.route('/logout/')
def logout():
    logout_user()
    return redirect(url_for('home'))


@app.route('/analyse/comparateur', methods=['GET', 'POST'])
@login_required
def page_analyse_comparateur():
    # Coder à l'aveugle comme si l'interface existait déjà
    liste_capteurs = Capteur.query.filter(Capteur.arduino == Arduino.id_arduino,
                                          Arduino.utilisateur == current_user.get_id()).all()
    if request.method == "POST":
        form_comparer = ComparerForm()
        if form_comparer.validate_on_submit():
            print(f"request.form.getlist('checklist_capteur') : {request.form.getlist('checklist_capteur')}")
            for capteur in liste_capteurs:
                if capteur.id_capteur in request.form.getlist("checklist_capteur"):
                    setattr(capteur, "coche", True)
                else:
                    setattr(capteur, "coche", False)
    return render_template("page_graphique.html", liste_capteurs=liste_capteurs)


@app.route('/graphique', methods=['GET'])
@login_required
def graphique():
    dico_donnees = {}
    if request.args.get("titre_graphique"):
        titre_graphique = request.args.get("titre_graphique")
    else:
        titre_graphique = "Titre du graphique"
    if request.args.get("debut"):
        date_debut = datetime.fromtimestamp(int(request.args.get("debut")) / 1000).replace(second=0, microsecond=0)
    else:
        date_debut = (datetime.now().replace(second=0, microsecond=0) - relativedelta(years=10))
    if request.args.get("fin"):
        date_fin = datetime.fromtimestamp(int(request.args.get("fin")) / 1000).replace(second=0, microsecond=0)
    else:
        date_fin = datetime.now().replace(second=0, microsecond=0)
    for id_capteur in request.args.getlist("id_capteur"):
        if Arduino.query.filter(Arduino.id_arduino == Capteur.arduino,
                                Capteur.id_capteur == id_capteur).first().utilisateur == current_user.get_id():
            nom_capteur = Capteur.query.get(id_capteur).nom_capteur
            donnees = Donnee_physique.query \
                .filter(Donnee_physique.id_capteur == id_capteur,
                        Donnee_physique.date_donnee.between(date_debut, date_fin)) \
                .order_by(Donnee_physique.date_donnee).all()
            if donnees:
                dico_donnees[id_capteur] = donnees
                dico_donnees[id_capteur].append(nom_capteur)
        else:
            flash("Vous avez tenté d'accéder aux données d'un capteur auxquelles vous n'avez pas accès", "danger")

    return render_template("graph.html", dico_donnees=dico_donnees, titre_graphique=titre_graphique)


@app.route('/analyse/tableau', methods=['GET'])
@login_required
def page_analyse_tableau():
    return render_template("tableau_resultat.html")


@app.route('/offres', methods=['GET'])
def page_offres():
    return 'offres'


@app.route('/groupe/arduino', methods=('GET', 'POST'))
@login_required
def page_arduino():
    form = ArduinoForm()
    if request.method == 'POST':
        if form.validate_on_submit:
            creer_arduino(form.nom_arduino.data, current_user.get_id())
        return render_template("tableau_resultat.html", title="Oublie de mot de passe")
    return render_template("ajout_arduino.html", form=form)


@app.route('/offres/basique', methods=['GET'])
def page_offres_basique():
    return 'offre basique'


@app.route('/offres/location', methods=['GET'])
def page_offres_location():
    return 'offre basique'


@app.route('/profil', methods=['GET', 'POST'])
@login_required
def page_profil():
    u = Utilisateur.query.get(current_user.pseudo_utilisateur)

    class ModifProfileForm(FlaskForm):
        """Classe de formulaire de modification du profil"""
        nom = StringField('Nom :', default=u.nom_utilisateur, validators=[DataRequired(), Length(min=2, max=32)])
        prenom = StringField('Prénom :', default=u.prenom_utilisateur,
                             validators=[DataRequired(), Length(min=2, max=32)])
        email = StringField("Email :", default=u.mail_utilisateur, validators=[DataRequired(), Email()])
        submit = SubmitField("Confirmer les modifications")
        next = HiddenField()
        mot_de_passe = PasswordField('Mot de passe', validators=[DataRequired(), Length(min=6, max=32)])
        confirm_mot_de_passe = PasswordField('Confirmer mot de passe',
                                             validators=[DataRequired(), Length(min=6, max=32),
                                                         EqualTo("mot_de_passe")])

    form = ModifProfileForm()
    if form.validate_on_submit():
        mdp = sha256()
        mdp.update(form.mot_de_passe.data.encode())
        u.nom_utilisateur = form.nom.data
        u.prenom_utilisateur = form.prenom.data
        u.mail_utilisateur = form.email.data
        u.mdp_utilisateur = mdp.hexdigest()
        db.session.commit()

        flash("Votre compte a été modifié avec succès.", "success")
        return redirect(url_for("page_profil"))

    groupes = get_groupes_utilisateur(current_user.get_id())
    form_quitter_groupe = QuitterGroupe()

    return render_template("profil.html", form=form, user=u, groupes=groupes, form_quitter_groupe=form_quitter_groupe)


@app.route('/profil/quitter_groupe/<id_groupe>', methods=['POST'])
@login_required
def quitter_groupe(id_groupe):
    form_quitter_groupe = QuitterGroupe()
    if form_quitter_groupe.validate_on_submit():
        retirer_utilisateur_groupe(pseudo_utilisateur=current_user.get_id(), id_groupe=id_groupe)
    return redirect(url_for("page_profil"))


def id_capteur_existe(id_capteur):
    if Capteur.query.get(id_capteur):
        return True
    return False


def id_arduino_existe(id_arduino):
    if Arduino.query.get(id_arduino):
        return True
    return False


def id_capteur_est_valide(id_capteur, type_variable):
    if not isinstance(id_capteur, type_variable):
        print(f"Problème : Le capteur qui a pour id \"{id_capteur}\" n'est pas de type \"{type_variable}\"")
        return False
    if not id_capteur_existe(id_capteur):
        print(f"Problème : Le capteur qui a pour id \"{id_capteur}\" n'existe pas dans la BD")
        return False
    return True


def id_arduino_est_valide(id_arduino, type_variable):
    if not isinstance(id_arduino, type_variable):
        print(f"Problème : L'arduino qui a pour id \"{id_arduino}\" n'est pas de type \"{type_variable}\"")
        return False
    if not id_arduino_existe(id_arduino):
        print(f"Problème : L'arduino qui a pour id \"{id_arduino}\" n'existe pas dans la BD")
        return False
    return True


def val_donnee_est_valide(val_donnee):
    if not isinstance(val_donnee, float):
        print(f"Problème : La valeur du champs val_donnee n'est pas un float. Voici la valeur : {val_donnee}")
        return False
    return True


def unite_donnee_est_valide(unite_donnee):
    if not isinstance(unite_donnee, str):
        print(f"Problème : La valeur du champs unite_donnee n'est pas un str. Voici la valeur : {unite_donnee}")
        return False
    return True


def nom_donnee_est_valide(nom_donnee):
    if not isinstance(nom_donnee, str):
        print(f"Problème : La valeur du champs nom_donnee n'est pas un str. Voici la valeur : {nom_donnee}")
        return False
    return True


def type_donnee_gps_est_valide(donnee_gps):
    if not isinstance(donnee_gps, float):
        print(
            f"Problème : La valeur du champs latitude ou longitude n'est pas un float. Voici la valeur : {donnee_gps}")
        return False
    return True


def syntaxe_donnees_valide(donnees, id_capteur):
    est_valide = True
    for cle, valeur in donnees.items():
        # Vérification que chaque champs d'une donnee dans le fichier n'est pas vide, None et n'est
        # pas un espace
        if valeur is None or valeur == "":
            est_valide = False
            print(f"Problème : La valeur \"{valeur}\" du champs \"{cle}\" du capteur d'id \"{id_capteur}\" "
                  f"est soit vide, soit None")
    return est_valide


def date_time_est_valide(date_time, precision_minimum):
    est_valide = True
    count_precision = 0
    for elem in date_time.split("-"):
        # Vérification que la date_time donnée est assez précise
        count_precision += 1
        if not elem.isdigit():
            est_valide = False
            print(f"Problème : La date \"{date_time}\" contient une erreur de syntaxe. "
                  f"Élément causant le problème : \"{elem}\"")
    if count_precision < precision_minimum:
        print(f"Problème : La date \"{date_time}\" n'est pas assez précise")
        return False
    return est_valide


def donnees_sont_valide(donnees, id_capteur):
    return syntaxe_donnees_valide(donnees, id_capteur) and unite_donnee_est_valide(donnees["unite_donnee"]) and \
           val_donnee_est_valide(donnees["val_donnee"]) and nom_donnee_est_valide(donnees["nom_donnee"]) \
           and date_time_est_valide(donnees["date_donnee"], 6)


def intervalle_donnee_gps_est_valide(donnee, type_donnee):
    if type_donnee == "latitude":
        return -90.0 <= donnee <= 90.0
    elif type_donnee == "longitude":
        return -180.0 <= donnee <= 180.0
    print(f"Problème : La donnee gps \"{donnee}\" n'est pas dans l'intervalle (-90,90 ou -180,180)")
    return False


def donnees_gps_sont_valide(donnees):
    return date_time_est_valide(donnees["date_donnee"], 6) and type_donnee_gps_est_valide(donnees["latitude"]) and \
           type_donnee_gps_est_valide(donnees["longitude"]) and \
           intervalle_donnee_gps_est_valide(donnees["latitude"], "latitude") and \
           intervalle_donnee_gps_est_valide(donnees["longitude"], "longitude")
