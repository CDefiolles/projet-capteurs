// let imported = document.createElement('script');
// imported.src = './map.js';
// console.log(imported.src)
// document.head.appendChild(imported);

let listeInput = document.getElementsByTagName("input");
for (let input of listeInput) {
    if (input.getAttribute("onclick").match("actualiser_graph")) {
        if (input.checked) {
            actualiser_map(input.id);
        }
    }
}

function actualiser_map(id_capteur) {
    let checkbox = document.getElementById(id_capteur)
    let iframe = document.getElementById("iframe_map");
    let src = iframe.src;
    if (checkbox.checked) {
        src += id_capteur + "&";
    } else {
        // À faire fonctionner (le replace ne fait pas son travail)
        console.log("Modèle: " + id_capteur + "&");
        console.log("src: " + src);
        let a_remplacer = id_capteur + "&";
        src = src.replace(a_remplacer, "");
        console.log(src);
    }
    iframe.src = src;
}

function actualiser_groupe_frame(id_groupe) {
    let iframe = document.getElementById("iframe_groupe");
    let src = iframe.src;
    let list = src.split('/')
    list.pop()
    list.push(id_groupe)
    src = list.join('/')
    iframe.src = src;
}