"""
Conception des requêtes sur la BD
"""
from datetime import datetime
from .models import *


def creer_groupe(nom_groupe, pseudo_administrateur, est_public=False):
    """Retourne l'id du groupe venant d'être crée"""
    admin = Utilisateur.query.get(pseudo_administrateur)
    groupe = Groupe(nom_groupe=nom_groupe, est_public=est_public, administrateurs=[admin])
    db.session.add(groupe)
    db.session.flush()  # flush permet d'envoyer la requête à la BD

    id_role = creer_role("Défaut", groupe.id_groupe)  # création du rôle par défaut du groupe

    # On ajoute le créateur du groupe aux membres de celui-ci
    ajouter_utilisateur_groupe(pseudo_administrateur, groupe.id_groupe, id_role)

    db.session.commit()  # commit permet d'effectuer/terminer la transaction en appliquant les changements

    return groupe.id_groupe


def modifier_add(table, **args):
    ligne = table.query.get(args["valeur_clef_primaire"])

    for attribut, valeur in args["dico_donnees"].items():
        getattr(ligne, attribut).append(valeur)
    db.session.commit()


def modifier_remove(table, **args):
    """
    Cette fonction permet de supprimer des liens entre des tables
    Les liens sont 'secondary many-to-many'
    Exemple : retirer l'un des capteurs d'un groupe
    """
    ligne = table.query.get(args["valeur_clef_primaire"])

    for attribut, valeur in args["dico_donnees"].items():
        getattr(ligne, attribut).remove(valeur)
    db.session.commit()


def creer_role(nom_role, id_groupe):
    """Retourne l'id du rôle venant d'être crée"""
    role = Role(nom_role=nom_role, id_groupe=id_groupe)
    db.session.add(role)
    db.session.commit()
    return role.id_role

def creer_arduino(nom_arduino, pseudo_utilisateur):
    """Retourne l'id de l'arduino venant d'être créée"""
    arduino = Arduino(nom_arduino=nom_arduino, utilisateur=pseudo_utilisateur)
    db.session.add(arduino)
    db.session.commit()

    return arduino.id_arduino


def creer_capteur(id_capteur, nom_capteur, arduino):
    capteur = Capteur(id_capteur=id_capteur, nom_capteur=nom_capteur, date_installation_capteur=datetime.now(),
                      arduino=arduino)
    db.session.add(capteur)
    db.session.commit()


def retirer_utilisateur_groupe(pseudo_utilisateur, id_groupe):
    lignes_appartenance_groupe_role = Utilisateur_appartient_groupe.query \
        .filter(Utilisateur_appartient_groupe.pseudo_utilisateur == pseudo_utilisateur,
                Utilisateur_appartient_groupe.id_groupe == id_groupe).all()
    # le même utilisateur peut appartenir à un même groupe mais sous différent rôle
    for ligne in lignes_appartenance_groupe_role:
        db.session.delete(ligne)
    db.session.commit()


def retirer_capteur_groupe(id_capteur, id_groupe):
    print("entered query retirer")
    capteur = Capteur.query.get(id_capteur)
    modifier_remove(Groupe, nom_clef_primaire="id_groupe", valeur_clef_primaire=id_groupe,
                    dico_donnees={"capteurs": capteur})


def add_role(id_role, id_utilisateur, id_groupe):
    nouvelle_relation = Utilisateur_appartient_groupe(pseudo_utilisateur=id_utilisateur, id_groupe=id_groupe,
                                                      id_role=id_role)
    db.session.add(nouvelle_relation)
    db.session.commit()


def get_all_arduino():
    return Arduino.query.all()


def ajouter_utilisateur_groupe(pseudo_utilisateur, id_groupe, id_role):
    utilisateur = Utilisateur.query.get(pseudo_utilisateur)
    groupe = Groupe.query.get(id_groupe)
    role = Role.query.get(id_role)
    association = Utilisateur_appartient_groupe(utilisateur=utilisateur, groupe=groupe, role=role)
    db.session.add(association)
    db.session.commit()


def get_groupes_utilisateur(pseudo_utilisateur):
    # recuperation des groupes auxquels l'utilisateur appartient + groupes public
    groupes_appartient = Groupe.query.filter(Utilisateur_appartient_groupe.pseudo_utilisateur == pseudo_utilisateur,
                                             Utilisateur_appartient_groupe.id_groupe == Groupe.id_groupe)
    groupes_public = Groupe.query.filter(Groupe.est_public is True)
    groupes = groupes_appartient.union(groupes_public)

    return groupes


def get_dico_groupe_capteurs(pseudo_utilisateur):
    # recuperation des groupes auxquels l'utilisateur appartient + groupes public
    groupes = get_groupes_utilisateur(pseudo_utilisateur)

    # creation d'un dico 'groupe : [capteurs]' pour la légende
    dico_groupe_capteurs = {}
    for groupe in groupes:
        dico_groupe_capteurs[groupe] = Capteur.query.join(Groupe.capteurs).filter(
            Groupe.id_groupe == groupe.id_groupe).all()

    return dico_groupe_capteurs


def get_dico_membres_roles_groupe(id_groupe):
    """
    Cette fonction renvoie un dictionnaire.
    Celui-ci a pour clés les membre du groupe associés à l'id_groupe
    et pour valeurs la liste des rôles du membre
    """
    # recuperation des membre du groupe
    membres_groupes = Utilisateur.query \
        .filter(Utilisateur_appartient_groupe.pseudo_utilisateur == Utilisateur.pseudo_utilisateur,
                Utilisateur_appartient_groupe.id_groupe == id_groupe).all()

    # creation d'un dico membre -> roles
    dico_dico_membres_role_groupe = {}
    for membre in membres_groupes:
        dico_dico_membres_role_groupe[membre] = Role.query.filter(
            Role.id_groupe == Utilisateur_appartient_groupe.id_groupe,
            Utilisateur_appartient_groupe.id_groupe == id_groupe,
            Utilisateur_appartient_groupe.pseudo_utilisateur == membre.pseudo_utilisateur).all()

    return dico_dico_membres_role_groupe

def get_role_user_pour_un_groupe(pseudo_utilisateur,id_groupe):
    """
    Permet de retourner le rôle d'un utilisateur dans un grouope
    """
    return Role.query.filter(Role.id_groupe==id_groupe,Utilisateur_appartient_groupe.id_groupe==id_groupe,Utilisateur_appartient_groupe.pseudo_utilisateur==pseudo_utilisateur).first()


def get_dico_droits_groupe_droit_actif(id_role):
    """
    Cette fonction renvoie un dictionnaire.
    Celui-ci a pour clés les droits (liés au groupe) associés à l'id_role
    et pour valeurs l'attribut 'droit_actif' de l'association droit_role_groupe qui indique l'état du droit pour ce rôle
    """
    # récupération des droits associés à ce rôle
    droits = Droit_groupe.query.filter(Droit_groupe.id_droit == Droit_role_groupe.id_droit_groupe,
                                       Droit_role_groupe.id_role == id_role)
    dico_droits_groupe_droit_actif = {}
    for droit in droits:
        # récupération de l'association entre le role (id_role) et le droit
        droit_role_groupe = Droit_role_groupe.query.filter(
            Droit_role_groupe.id_droit_groupe == droit.id_droit,
            Droit_role_groupe.id_role == id_role).first()

        # attribution à la clé 'droit' la valeur 'droit_actif'
        dico_droits_groupe_droit_actif[droit] = droit_role_groupe.droit_actif

    return dico_droits_groupe_droit_actif


def get_capteurs_par_groupe(id_groupe):
    detail_groupe = Groupe.query.get(id_groupe)
    return detail_groupe.capteurs

def retirer_role_user_pour_un_groupe(pseudo_utilisateur,id_groupe,id_role):
    ligne_a_retirer=Utilisateur_appartient_groupe.query.filter(id_role==id_role,id_groupe == id_groupe,pseudo_utilisateur == pseudo_utilisateur).first()
    print(ligne_a_retirer)
    db.session.delete(ligne_a_retirer)
    db.session.commit()
